#!/bin/bash

sudo mkfs.ext4 /dev/sda
sudo mkdir /mnt/ssd
sudo mount /dev/sda /mnt/ssd
cp /etc/fstab /etc/fstab.backup
echo "/dev/sda /mnt/ssd ext4 defaults 0 0" >> /etc/fstab
echo "Drive setup complete."

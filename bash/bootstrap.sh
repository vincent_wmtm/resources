#!/bin/bash

new_hostname="$1"
dash_hostname="${new_hostname//./-}"

mkdir /etc/systemd/resolved.conf.d/
echo -e "[Resolve]\nDNS=192.168.88.247" > /etc/systemd/resolved.conf.d/dns_servers.conf
systemctl restart systemd-resolved

sed -i 's/#NTP=/NTP=ntp.lan/g' /etc/systemd/timesyncd.conf

new_host_entries="127.0.0.1 ${new_hostname}\n127.0.0.1 ${dash_hostname}"
echo -e "${new_host_entries}" > ~/hostnames.update.tmp
cat /etc/hosts >> ~/hostnames.update.tmp
mv ~/hostnames.update.tmp /etc/hosts
hostnamectl set-hostname ${new_hostname}

sed -i 's/preserve_hostname: false/preserve_hostname: true/g' /etc/cloud/cloud.cfg
echo "Config complete, please reboot now."
